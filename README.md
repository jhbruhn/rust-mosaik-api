# Rust Mosaik API

This is a rust implementation of the [mosaik client API](https://mosaik.readthedocs.io/en/latest/mosaik-api/low-level.html). It is intended to be a library to use mosaik in rust.

## Development

This repository is infrequently in development.
Currently, only the initialization test is working. 

## Current development goals

To reach 1.0, the following points must still be implemented.

- Refactoring/restructuring of the current code
- Tests for all API functions
- Server implementation
- Integration test with a mosaik server
- Release lib to cargo

## License 

This project is under the MIT License, see file for details.