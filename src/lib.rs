use std::net::{TcpListener, TcpStream};
use std::io::{BufReader, Read, self};
use std::collections::HashMap;
use std::str::FromStr;
use serde::Deserialize;
use serde_json::{Value, json};

#[derive(Deserialize, Debug)]
pub enum MosaikMethod {
    init,
    create,
    setup_done,
    step,
    get_data,
    //async_get_progress, // Not implementing
    //async_get_related_entities,
    //async_get_data,
    //async_set_data,
    //async_set_event,
}

#[derive(Debug)]
pub struct Message {
    header: usize,
    payload: Payload,
}

impl Message {
    pub fn parse_request(mut buf_reader: BufReader<&mut dyn Read>) -> Message {
        let mut buffer: [u8; 4] = [0; 4];
        buf_reader.read_exact(&mut buffer).unwrap();

        let mut s = String::new();
        buf_reader.read_to_string(&mut s).expect("Buffer should not be empty or else not readable");

        Message{
            header: bits_to_usize(&buffer),
            payload: Payload::new(s)
        }
    }
    
    pub fn from_payload(payload: Payload) -> Message {
        let m = match payload.content {
            MessageWithType::Request(r) => panic!("Request is not implemented!"),
            MessageWithType::Response(r) => {
                let u = r.content.capacity();
                Message { 
                    header: u,
                    payload: Payload { message_type: payload.message_type, id: payload.id, content: MessageWithType::Response(Response { content: String::from_str(&r.content).unwrap()}) }
                }
            }
        };
        m
    }
}

#[derive(Debug)]
pub enum MessageWithType {
    Request(Request),
    Response(Response)
}

#[derive(Debug)]
pub struct Payload {
    message_type: usize,
    id: usize,
    content: MessageWithType,//Request
}

impl Payload {
    pub fn new(json: String) -> Payload{
        let value: Value = serde_json::from_str(&json).expect("Cannot read json file");
        let array = value.as_array().unwrap();
        Payload {
            message_type: serde_json::from_value(array[0].clone()).unwrap(),
            id: serde_json::from_value(array[1].clone()).unwrap(),
            content: MessageWithType::Request(serde_json::from_value(array[2].clone()).unwrap())
        }
    }
}

#[derive(Deserialize, Debug)]
pub struct Request {
    function: MosaikMethod,
    args: Vec<String>,
    kwargs: HashMap<String, Value>,
}

impl Request {
    pub fn new(_s: &mut String) -> Request {
        Request {
            function: MosaikMethod::init,
            args: vec![],
            kwargs: HashMap::new(),
        }
    }
}

#[derive(Deserialize, Debug)]
pub struct Response {
    content: String,
}

pub fn bits_to_u32(input: &[u8; 4]) -> u32 {
    let mask: u8 = 0b0000_0111;
    let masked_out_value: u8 = &input[0].clone() & mask;
    let value = u32::from_be_bytes([masked_out_value, 0, 0, 0]);
    value
}

pub fn bits_to_usize(input: &[u8; 4]) -> usize {
    let mask: u8 = 0b0000_0111;
    let masked_out_value: u8 = &input[0].clone() & mask;
    let value = usize::from_be_bytes([masked_out_value, 0, 0, 0, 0, 0, 0, 0]);
    value
}

pub trait Entity {
    fn get_entity_id(&self) -> String;
    fn get_type(&self) -> String;
    fn get_rel(&self) -> Vec<String>;
    fn get_children<'a>(&self) -> Vec<&'a dyn Entity>;
    fn to_string(&self) -> String;
}

pub trait MosaikMetadata {
    fn get_metadata(&self) -> String;
}


pub trait Simulator<'a> {
    fn initialize(&'a self, sim_id: String, time_resolution: f64, sim_params: &HashMap<String, Value>) -> Result<&'a dyn MosaikMetadata, String>;
    fn create(&'a mut self) -> Result<Vec<&'a dyn Entity>, String>;
    fn setup_done(&'a self) -> Result<(), io::Error>;
    fn step(&'a self, time: f64, input: Value, max_advance: f64) -> Result<f64, String>;
    fn get_data(&self) -> Result<serde_json::Value, serde_json::Error>;
}

pub struct SimServer<'a> {
    simulator: &'a mut dyn Simulator<'a>
}

impl<'a> SimServer<'a> {
    pub fn start(&self, binding: Option<&str>, port: Option<i64>) -> Result<(), io::Error>{
        Ok(())
    }

    fn parse_request(&'a mut self, request: &Request) -> Result<Response, io::Error>{
        let f = match request.function {
            MosaikMethod::init => {
                let sim_id = request.args[0].parse().unwrap();
                let default = 1f64;
                let time = request.kwargs.get("time_resolution").map_or(default, |x| x.as_f64().map_or(default, |y| y));//.as_f64().take(); //["time_resolution"].as_f64().unwrap_or(1f64);
                let s = self.simulator.initialize(sim_id, time, &request.kwargs).unwrap();
                let result = s.get_metadata();
                Response {
                    content: String::from(result)
                }
            }
            MosaikMethod::create => {
                let mut entities = self.simulator.create()
                    .unwrap();
                let mut e = entities.iter();
                let m = e
                    .map(|entity| entity.to_string())
                    .map(|entity_str| serde_json::from_str(&entity_str).unwrap())
                    .collect();
                Response {
                    content: serde_json::Value::Array(m).to_string()
                }
            },
            MosaikMethod::setup_done => {
                self.simulator.setup_done().unwrap();
                Response {content: serde_json::Value::Null.to_string()}
            }
            MosaikMethod::step => {
                let step = self.simulator.step(request.args[0].parse().unwrap(), serde_json::from_str(&request.args[1]).unwrap(), request.args[2].parse().unwrap()).unwrap();
                Response {
                    content: step.to_string()
                }
            }
            MosaikMethod::get_data => {
                let s = self.simulator.get_data().unwrap();
                Response {
                    content: s.to_string()
                }
            }
        };
        //f()
        Ok(f)
    }
}



#[cfg(test)]
mod tests {

    use super::*;
    use std::io::Cursor;
    use std::collections::HashMap;
    use std::str::FromStr;

    const INIT_CALL: &str = "\x00\x00\x00\x36[0, 1, [\"init\", [\"hello\", \"world\"], {\"times\": 23}]]";
    const CREATE_CALL: &str = "\x00\x00\x00\x36[0, 1, [\"init\", [\"hello\", \"world\"], {\"times\": 23}]]";
    const STEP_CALL: &str = "\x00\x00\x00\x36[0, 1, [\"init\", [\"hello\", \"world\"], {\"times\": 23}]]";
    const GET_DATA_CALL: &str = "\x00\x00\x00\x36[0, 1, [\"init\", [\"hello\", \"world\"], {\"times\": 23}]]";
    const SETUP_DONE_CALL: &str = "\x00\x00\x00\x36[0, 1, [\"init\", [\"hello\", \"world\"], {\"times\": 23}]]";

    struct TestEntity {}

    impl Entity for TestEntity {
        fn get_children<'a>(&self) -> Vec<&'a dyn Entity> {
            let mut e : Vec<&'a dyn Entity> = vec![];
            e
        }
        fn get_entity_id(&self) -> String {
            String::from_str("foo").unwrap()
        }
        fn get_rel(&self) -> Vec<String> {
            vec![String::from_str("foo").unwrap()]
        }
        fn get_type(&self) -> String {
            String::new()
        }

        fn to_string(&self) -> String {
            todo!()
        }
    }

    struct TestSim {
        entities: Vec<TestEntity>
    }

    impl MosaikMetadata for TestSim {
        fn get_metadata(&self) -> String {
            String::from_str("{
                'type': 'time-based',
                'models' {
                    'A': {
                          'public': True,
                          'params': ['init_val'],
                          'attrs': ['val_out', 'dummy_out'],
                    },
                }").unwrap()
        }
    }

    impl<'a> Simulator<'a> for TestSim  {
        fn initialize(&'a self, sim_id: String, time_resolution: f64, sim_params: &HashMap<String, Value>) -> Result<&'a dyn MosaikMetadata, String> {
            println!("Simid: {}", sim_id);
            for m in sim_params {
                println!("({}, {})", m.0, m.1);
            }
            Ok(self)
        }
        fn create(&'a mut self) -> Result<Vec<&'a dyn Entity>, String> {
            
            self.entities.insert(0, TestEntity {});
            let mut v : Vec<&'a dyn Entity> = vec![];

            for e in self.entities.iter() {
                v.insert(v.len(), e as &dyn Entity)
            }
            Ok(v)
        }

        fn get_data(&self) -> Result<serde_json::Value, serde_json::Error> {
            serde_json::from_str("{
                'sid_0.eid_0': {
                    'sid_1.eid_0': {'type': 'B'},
                },
                'sid_0.eid_1': {
                    'sid_1.eid_1': {'type': 'B'},
                },
            }")
        }

        fn setup_done(&self) -> Result<(), io::Error> {
            Ok(())
        }

        fn step(&self, time: f64, input: Value, max_advance: f64) -> Result<f64, String> {
            Ok(time+1f64)
        }
    }

    #[test]
    fn it_works() {
        //let listener = TcpListener::bind("127.0.0.1:80").unwrap();
        //for stream in listener.incoming() {
            //let mut stream = stream.unwrap();
            let mut cursor = Cursor::new(INIT_CALL);
            let buf_reader: BufReader<&mut dyn Read> = BufReader::new(&mut cursor);
            let r = Message::parse_request(buf_reader);
            assert_eq!(r.payload.message_type, 0);

            let mut t = TestSim { entities: vec![]};
            let metadata = t.get_metadata();
            let mut s = SimServer { simulator: &mut t };
            

            match &r.payload.content {
                MessageWithType::Request(request) => {
                    assert!(matches!(request.function, MosaikMethod::init));
                    let response = Response { content: metadata };
                    let parsed_response = s.parse_request(request).unwrap();
                    assert_eq!(parsed_response.content, response.content);
                },
                MessageWithType::Response(_) => assert!(false)
            }
            
            
        //}
        println!("Serialized input: {:?}", r);
    }
}
